<?php

$ip = 'unknow';

if (isset($_SERVER['SERVER_ADDR'])) {
    $ip = $_SERVER['SERVER_ADDR'];
}

if (isset($_SERVER['HTTP_X_REAL_IP'])) {
    $ip = $_SERVER['HTTP_X_REAL_IP'];
}

if (empty($_GET['v'])) {
    echo 'Proxy Boy v0.2 :🚶‍♂️';
    echo PHP_EOL;
    echo "IP: {$ip}";
    die;
}

$baseUrl = 'https://www.youtube.com/watch?v=';
$url = $baseUrl . $_GET['v'];

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);

if (!empty($_GET['v'])) {
    $content = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($code !== 200) {
        http_response_code($code);
        echo 'fail ' . $code;
        echo "<br/>";
        echo $content;
        die;
    }

    echo $content;
}